let mongoose = require('mongoose')

let rsvpSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    attending:{
        type: String,
        required: true
    },
    numberAttending: {
        type: Number,
        required: true
    }
})

let Rsvp = module.exports = mongoose.model("Rsvp", rsvpSchema)