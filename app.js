//self reminder "Express in a routing and middleware web fraework that has minimal
//functionality of its own: an Express app id essentially a series of middleware function calls
//cycle 
// middleware functions are functions that have access to the request object(req), (res), and the next middleware func in the apps req/res c
const express = require('express');
const path = require("path");
const mongoose = require('mongoose');

//have to have body-parser to parse the body of incoming requests
const bodyParser = require('body-parser');

//"app" object conventionally denotes the express application. Create it by 
// calling the top-level
const app = express();
//app.use is middleware in this case its 
app.use(express.static(path.join(__dirname, 'public')))


app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }))

//connect to databse
mongoose.connect('mongodb://localhost/rsvp', {useNewUrlParser: true})
let db = mongoose.connection

//check connection
db.once('open', function(){})

//check for db errors
db.on('error', function(err){
    console.log(err)
})

//bring in models 
let Rsvp = require('./models/rsvp')

//set view folder 
app.set('views', path.join(__dirname, "views"));
app.set('view engine', 'pug');

//home route
app.get('/', function(req, res){
    //instead of res.send we use res.render to render the view engine
    //we can also pass values into our view engine 
    Rsvp.find({}, function(err, rsvps){
        if(err){
            console.log(err)
        }else {
        res.render('index', {
           actionPath: '/rsvps/add'
        })
    }
    })
})

//add route 
app.post('/rsvps/add', function(req, res) {
   console.log(req.body)
   let newEntry = new Rsvp({
       name: req.body.name,
       email: req.body.email,
       attending: req.body.attending,
       numberAttending: req.body.number
   })
   newEntry.save()

   res.render('reply',{})
})

app.get('/guests', (req, res) => {
    Rsvp.find({}, (err, responses) => {
      console.log(responses)
      let attendArr = []
      let notAttendArr = []
      for (item of responses) {
        if (item.attending === 'yes') {
          attendArr.push(item)
        } else {
          notAttendArr.push(item)
        }
      }
      res.render('guests', { 
        attending: attendArr, 
        notAttending: notAttendArr 
      })
    })
    // console.log(currentDB)
    // res.send(currentDB)
  })

//start server 
app.listen(3000, function() {
    console.log('Server listening on port 3000...')
})